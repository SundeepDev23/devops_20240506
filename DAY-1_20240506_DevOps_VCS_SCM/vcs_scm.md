#### Session Video 

```
https://drive.google.com/file/d/1G-vefy6kj9HP4U4H0pClkfaxrx4lPrWS/view?usp=sharing
```

#### Version Control(VCS) / Source Code Management(SCM) - GIT

```
1. Getting Started with VCS/SCM

2. What is Git, AWS CodeCommit, and GitHub?

3. About Version Control Systems and Types

4. Git Workflow

5. Installing Git on Windows & Linux

6. Getting Started with Git Commands

7. Working with Branches

8. Merging Branches

9. Creating and Committing a Pull Request

10. Working with Stash

11. Summary 
```

#### 1. Getting Started with VCS/SCM

```

```